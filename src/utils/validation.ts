import * as yup from 'yup';

export const LoginValidation = yup.object().shape({
  email: yup
    .string()
    .email('Email must in the correct format')
    .required('Email is a required field'),
  password: yup
    .string()
    .min(8, 'Password must be 8 characters long')
    .matches(/[0-9]/, 'Password requires a number')
    .matches(/[a-z]/, 'Password requires a lowercase letter')
    .matches(/[A-Z]/, 'Password requires an uppercase letter'),
});

export const RegisterValidation = yup.object().shape({
  name: yup.string().required('Name is a required field'),
  email: yup
    .string()
    .email('Email must in the correct format')
    .required('Email is a required field'),
  password: yup
    .string()
    .min(8, 'Password must be 8 characters long')
    .matches(/[0-9]/, 'Password requires a number')
    .matches(/[a-z]/, 'Password requires a lowercase letter')
    .matches(/[A-Z]/, 'Password requires an uppercase letter'),
  age: yup.number().required('Age is a required field'),
});

export const UpdateProfileValidation = yup.object().shape({
  name: yup.string().required('Name is a required field'),
  email: yup
    .string()
    .email('Email must in the correct format')
    .required('Email is a required field'),
  age: yup.number().required('Age is a required field'),
});

export const AddTask = yup.object().shape({
  description: yup.string().required('Task name is a required field'),
});
