import moment from 'moment';

const DEFAULT_FORMAT = 'MMMM Do YYYY, h:mm:ss a';

export const dateFormater = (
  date: Date | string | number | undefined,
  format: string = DEFAULT_FORMAT
) => moment(date).format(format);
