import axios from 'axios';

const BASE_URL = 'https://api-nodejs-todolist.herokuapp.com/';
let contextToken = '';

export const setToken = (accessToken: string) => {
  contextToken = accessToken;
};

const customAxios = axios.create({
  baseURL: BASE_URL,
});

customAxios.interceptors.request.use((config) => {
  const token = JSON.parse(localStorage.getItem('token') as string);
  let usedToken = token || contextToken;
  if (config.headers?.Authorization) {
    usedToken = config.headers.Authorization as string;
  }

  config.headers = {
    ...config.headers,
    Authorization: `Bearer ${usedToken}`,
  };
  return config;
});

customAxios.interceptors.response.use(
  function (response) {
    return response;
  },
  async function (error) {
    const originalRequest = error.config;

    if (error.response.status === 401 && !originalRequest._retry) {
      window.location.href = '/';
    }
    return Promise.reject(error);
  }
);

export default customAxios;
