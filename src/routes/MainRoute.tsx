import { Route, Routes } from 'react-router-dom';
import { Login, Register, Dashboard, Page404, Profile } from '@/pages';
import { ProtectedRoute } from '@/components/ProtectedRoute';

const MainRoutes = () => (
  <Routes>
    <Route path="/" element={<Login />} />
    <Route path="/register" element={<Register />} />
    <Route
      path="/dashboard"
      element={
        <ProtectedRoute>
          <Dashboard />
        </ProtectedRoute>
      }
    />
    <Route
      path="/profile"
      element={
        <ProtectedRoute>
          <Profile />
        </ProtectedRoute>
      }
    />

    <Route path="*" element={<Page404 />} />
  </Routes>
);

export default MainRoutes;
