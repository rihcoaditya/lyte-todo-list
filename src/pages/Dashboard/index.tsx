import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Avatar, Button, Layout, message, Row, Tabs, Typography } from 'antd';
import { UserOutlined } from '@ant-design/icons';

import { FlexBox } from '@/components';
import Tab from './components/tab';
import ModalAddTask from './components/ModalAddTask';
import { useLocalStorage } from '@/hooks/useLocalStorage';
import useDisclosure from '@/hooks/useDisclosure';
import { useWindowSize } from '@/hooks/useWindowSize';
import { useQueryGetTask } from '@/services/Task';

import { DashboardLayout, DashboardTasks, DashboardDivider } from './styled';

const { Content } = Layout;
const { Title, Text } = Typography;

const Dashboard = () => {
  const navigate = useNavigate();
  const [user] = useLocalStorage('user', null);
  const [avatar] = useLocalStorage('avatar', null);
  const useModalAddTask = useDisclosure();
  const { width } = useWindowSize();

  const [selectedTab, setSelectedTab] = useState('1');

  const { data, refetch, isLoading } = useQueryGetTask({
    onError: () => {
      message.error(
        'Something when wrong in the server please refresh the page or try again later.'
      );
    },
  });

  const tabItems = [
    { label: 'All Task' },
    { label: 'Today' },
    { label: 'Yesterday' },
    { label: 'Completed' },
    { label: 'Uncomplete' },
  ];

  return (
    <DashboardLayout>
      <Content className="pt-5 mb-5">
        <Row justify="center" gutter={[10, 10]}>
          <DashboardTasks xs={22} sm={18} md={16}>
            <FlexBox justifyContent="space-between" alignItems="center">
              <FlexBox alignItems="center">
                <Avatar
                  src={avatar}
                  icon={<UserOutlined />}
                  onClick={() => navigate('/profile')}
                  className={'cursor-pointer'}
                />
                <Text className="pl-1">{user.name}</Text>
              </FlexBox>
              <Title level={2}>My Tasks</Title>
              <Button type="primary" onClick={() => useModalAddTask.onOpen()}>
                Add Task
              </Button>
            </FlexBox>
            <DashboardDivider />
            <Tabs
              defaultActiveKey={'1'}
              tabPosition={(width && (width < 750 ? 'top' : 'left')) || 'left'}
              onChange={(activeKey) => setSelectedTab(activeKey)}
              items={tabItems.map((item, index) => {
                const id = String(index);
                return {
                  label: item.label,
                  key: id,
                  children: (
                    <Tab
                      refetch={refetch}
                      selectedTab={selectedTab}
                      list={data?.data || []}
                      isLoading={isLoading}
                    />
                  ),
                };
              })}
            />
          </DashboardTasks>
        </Row>
      </Content>
      <ModalAddTask
        visible={useModalAddTask.isOpen}
        refetchTasks={refetch}
        onClose={useModalAddTask.onClose}
      />
    </DashboardLayout>
  );
};

export default Dashboard;
