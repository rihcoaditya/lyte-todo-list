import styled from 'styled-components';
import { Button, Col, Divider } from 'antd';

const ModalAddTaskLayout = styled.div``;

const ModalAddTaskCol = styled(Col)`
  background-color: #ffffff;
  padding: 20px !important;
  border-radius: 15px;
  box-shadow: 0px 0px 10px rgba(25, 25, 25, 0.3);
  margin-top: 20px;
`;

const ModalAddTaskDivider = styled(Divider)`
  margin-top: 12px;
  border-top: 2px solid rgba(0, 0, 0, 0.76);
`;

const ModalAddTaskButton = styled(Button)`
  width: 100%;
`;

export {
  ModalAddTaskLayout,
  ModalAddTaskCol,
  ModalAddTaskDivider,
  ModalAddTaskButton,
};
