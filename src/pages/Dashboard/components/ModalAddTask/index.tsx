import React, { useEffect } from 'react';
import { Form, message, Modal } from 'antd';
import { useForm, FormProvider } from 'react-hook-form';

import { FieldTextInput } from '@/components';
import { AddTask } from '@/utils/validation';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutationPostTask } from '@/services/Task';

import { ModalAddTaskButton } from './styled';

import type { AxiosError } from 'axios';
import type { IPostTasks } from '@/services/Task/type';

interface IModalAddTask {
  visible?: boolean;
  refetchTasks: () => void;
  onClose: () => void;
}

const ModalAddTask = ({
  visible = false,
  refetchTasks,
  onClose,
}: IModalAddTask) => {
  const methods = useForm<IPostTasks>({
    mode: 'onChange',
    resolver: yupResolver(AddTask),
  });

  useEffect(() => {
    methods.reset();
  }, [visible]);

  const { mutate: mutateTask, isLoading: isLoadingTask } = useMutationPostTask({
    onSuccess: () => {
      message.success('Success create task');
      onClose();
      refetchTasks();
    },
    onError: (error: AxiosError) => {
      message.error(error?.message || 'Failed Login');
    },
  });

  const onSubmit = (values: IPostTasks) => {
    mutateTask(values);
  };

  return (
    <>
      <Modal
        visible={visible}
        title="Add Task"
        onCancel={!isLoadingTask ? onClose : undefined}
        footer={false}
      >
        <FormProvider {...methods}>
          <Form onFinish={methods.handleSubmit(onSubmit)}>
            <FieldTextInput
              label="Task name"
              name="description"
              placeholder="Input Task Name"
            />
            <ModalAddTaskButton
              loading={isLoadingTask}
              htmlType="submit"
              type="primary"
            >
              Add Task
            </ModalAddTaskButton>
          </Form>
        </FormProvider>
      </Modal>
    </>
  );
};

export default ModalAddTask;
