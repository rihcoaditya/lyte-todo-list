/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState } from 'react';
import {
  Col,
  Layout,
  Row,
  Typography,
  List,
  Tag,
  Popconfirm,
  Skeleton,
  message,
  Empty,
} from 'antd';
import { CheckCircleFilled, DeleteFilled } from '@ant-design/icons';
import { remove, isEmpty } from 'lodash';
import moment from 'moment';

import { FlexBox } from '@/components';
import { useWindowSize } from '@/hooks/useWindowSize';
import { dateFormater } from '@/utils/dateFormater';
import {
  useMutationCompleteTask,
  useMutationDeleteTask,
} from '@/services/Task';

import { TabLayout, TabContentBox } from './styled';

import type { IPostTasksResponse } from '@/services/Task/type';

const { Content } = Layout;
const { Text } = Typography;

const Tab = ({
  refetch,
  selectedTab,
  list,
  isLoading,
}: {
  refetch: () => void;
  selectedTab: string;
  list: IPostTasksResponse[] | [];
  isLoading: boolean;
}) => {
  const [selectedTask, setSelectedTask] = useState<
    IPostTasksResponse | Record<string, unknown>
  >({});
  const { width } = useWindowSize();

  const listTask = () => {
    const data = [...list];
    switch (selectedTab) {
      case '1':
        return remove(data, (task) =>
          moment(dateFormater(task.createdAt, 'YYYY-MM-DD')).isSame(
            dateFormater(new Date(), 'YYYY-MM-DD')
          )
        );
      case '2': {
        return remove(data, (task) => {
          const date = new Date();
          return moment(dateFormater(task.createdAt, 'YYYY-MM-DD')).isSame(
            dateFormater(date.setDate(date.getDate() - 1), 'YYYY-MM-DD')
          );
        });
      }
      case '3':
        return remove(data, (task) => task.completed === true);
      case '4':
        return remove(data, (task) => task.completed === false);
      default:
        return data;
    }
  };

  const { mutate: mutateCompleteTask, isLoading: isLoadingCompleteTask } =
    useMutationCompleteTask((selectedTask?._id || '') as string);

  const { mutate: mutateDeleteTask, isLoading: isLoadingDeleteTask } =
    useMutationDeleteTask((selectedTask?._id || '') as string, {
      onSuccess: () => {
        refetch();
        message.success(`You have deleted task ${selectedTask.description}`);
      },
    });

  const onDeleteTask = () => {
    mutateDeleteTask(undefined);
  };

  const onCompleteTask = () => {
    mutateCompleteTask(
      { completed: true },
      {
        onSuccess: () => {
          refetch();
          message.success(
            `Congrats you have completed task ${selectedTask.description}`
          );
        },
      }
    );
  };

  return (
    <TabLayout>
      <Content className="mb-5">
        <Row justify="center" gutter={[10, 10]}>
          <Col span={24}>
            {!isLoading && isEmpty(listTask()) ? (
              <Empty />
            ) : (
              <>
                {isLoading || isLoadingCompleteTask || isLoadingDeleteTask ? (
                  <Skeleton active />
                ) : (
                  <List
                    bordered
                    dataSource={listTask()}
                    renderItem={(item) => (
                      <List.Item onClick={() => setSelectedTask(item)}>
                        <TabContentBox resize={width}>
                          <FlexBox
                            direction="column"
                            style={{ maxWidth: 'calc(100% - 170px)' }}
                          >
                            <Text>{item.description}</Text>

                            {item.completed ? (
                              <Text ellipsis>
                                Finished On {dateFormater(item.updatedAt)}
                              </Text>
                            ) : (
                              <Text ellipsis>
                                {dateFormater(item.createdAt)}
                              </Text>
                            )}
                          </FlexBox>
                          <FlexBox alignItems="center">
                            {item.completed ? (
                              <Tag color="green">Completed</Tag>
                            ) : (
                              <>
                                <Tag color="red">Uncomplete</Tag>

                                <Popconfirm
                                  placement="leftBottom"
                                  title={'Change task status to completed?'}
                                  onConfirm={() => onCompleteTask()}
                                  okText="Yes"
                                  cancelText="No"
                                >
                                  <CheckCircleFilled
                                    style={{
                                      color: '#06ff3c',
                                      fontSize: '24px',
                                      paddingRight: '8px',
                                    }}
                                  />
                                </Popconfirm>
                              </>
                            )}
                            <Popconfirm
                              placement="leftBottom"
                              title={'Are you sure to delete this task?'}
                              onConfirm={() => onDeleteTask()}
                              okText="Yes"
                              cancelText="No"
                            >
                              <DeleteFilled
                                style={{ color: '#ff2000', fontSize: '24px' }}
                              />
                            </Popconfirm>
                          </FlexBox>
                        </TabContentBox>
                      </List.Item>
                    )}
                  />
                )}
              </>
            )}
          </Col>
        </Row>
      </Content>
    </TabLayout>
  );
};

export default Tab;
