import styled from 'styled-components';
import { FlexBox } from '@/components';

const TabLayout = styled.div`
  .ant-list {
    border: none;
    .ant-list-item {
      padding: 12px;
    }
  }
`;

const TabContentBox = styled(FlexBox)<{ resize?: number }>`
  width: 100%;
  align-items: center;
  justify-content: space-between;
  ${(props) =>
    props?.resize &&
    props.resize < 750 &&
    `
    flex-direction: column;
    gap: 10px;
    div {
      width: 100%;
      max-width: 100% !important;
    }
  `}
`;

export { TabLayout, TabContentBox };
