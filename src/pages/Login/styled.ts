import styled from 'styled-components';
import { Button, Layout } from 'antd';

const LoginLayout = styled(Layout)`
  padding: 20px;
  .ant-layout-content {
    align-items: center;
    justify-content: center;
    display: flex;
    flex-direction: column;
    .ant-row {
      width: 100%;
      .link-register {
        text-align: center;
        margin-top: 20px;
      }
    }
  }
`;

const LoginButton = styled(Button)`
  width: 100%;
  margin-top: 20px;
`;

export { LoginLayout, LoginButton };
