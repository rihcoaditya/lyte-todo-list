/* eslint-disable react-hooks/rules-of-hooks */
import React from 'react';
import {
  Col,
  Layout,
  Row,
  Typography,
  Image,
  message,
  Button,
  Avatar,
} from 'antd';
import { FormProvider, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';
import { UserOutlined } from '@ant-design/icons';

import { LoginValidation } from '@/utils/validation';
import { Form, FieldTextInput, FlexBox } from '@/components';
import { LogoTitle } from '@/Assets';
import { useLogin, useLogout } from '@/services/Login';
import { useQueryGetProfilePicture } from '@/services/Profile';
import { useLocalStorage } from '@/hooks/useLocalStorage';

import { LoginLayout, LoginButton } from './styled';

import type { ILoginFormValues } from './type';
import type { AxiosError } from 'axios';
import type { ILoginResponse } from '@/services/Login/type';

const { Content } = Layout;
const { Text, Link } = Typography;

const Login = () => {
  const [user, setUser] = useLocalStorage('user', null);
  const [token, setToken] = useLocalStorage('token', null);
  // eslint-disable-next-line no-unused-vars
  const [avatar, setAvatar] = useLocalStorage('avatar', null);
  const navigate = useNavigate();
  const methods = useForm<ILoginFormValues>({
    mode: 'onChange',
    resolver: yupResolver(LoginValidation),
  });

  const { mutate: mutateLogin, isLoading: isLoadingLogin } = useLogin({
    onSuccess: (data: ILoginResponse) => {
      setUser(data.user);
      setToken(data.token);
      message.success('Successfully Login.');
      navigate('/dashboard');
    },
    onError: (error: AxiosError) => {
      message.error(error?.message || 'Failed Login');
    },
  });

  const { mutate: mutateLogout, isLoading: isLoadingLogout } = useLogout({
    onSuccess: () => {
      setUser(null);
      setToken(null);
      setAvatar(null);
      message.success('Successfully Logout.');
    },
    onError: (error: AxiosError) => {
      message.error(error?.message || 'Failed Logout');
    },
  });

  const { isLoading: isLoadingProfilePicture } = useQueryGetProfilePicture(
    {
      userId: user?._id as string | '',
    },
    {
      enabled: !!token && !!user,
      onSuccess: (data: any) => {
        setAvatar(data);
      },
    }
  );

  const onSubmit = (values: ILoginFormValues) => {
    mutateLogin(values);
  };

  return (
    <LoginLayout>
      <Content className="mb-5">
        <Row justify="center" gutter={[10, 10]}>
          <Col xs={24} sm={16} md={12} lg={8} xl={6}>
            <FlexBox className="mb-5" justifyContent="center">
              <Image width={200} src={LogoTitle} />
            </FlexBox>
            {!token && !user ? (
              <FormProvider {...methods}>
                <Form onFinish={methods.handleSubmit(onSubmit)}>
                  <FieldTextInput
                    label="Email"
                    name="email"
                    placeholder="Input Email"
                  />
                  <FieldTextInput
                    type="password"
                    label="Password"
                    name="password"
                    placeholder="Input Password"
                  />
                  <LoginButton
                    loading={isLoadingLogin || isLoadingProfilePicture}
                    htmlType="submit"
                    type="primary"
                  >
                    Login
                  </LoginButton>
                </Form>
              </FormProvider>
            ) : (
              <FlexBox
                direction="column"
                alignItems="center"
                justifyContent="center"
              >
                <Avatar src={avatar} icon={<UserOutlined />} />
                <Button
                  className="mt-2"
                  type="primary"
                  htmlType="button"
                  loading={isLoadingLogout}
                  onClick={() => mutateLogout(undefined)}
                >
                  Logout
                </Button>
              </FlexBox>
            )}
          </Col>
        </Row>
        <Row justify="center">
          <Col xs={24} sm={16} md={12} lg={8} xl={6}>
            <FlexBox justifyContent="center">
              <Text className="link-register">
                or Signup <Link href="/register">here</Link>
              </Text>
            </FlexBox>
          </Col>
        </Row>
      </Content>
    </LoginLayout>
  );
};

export default Login;
