export interface IRegisterFormValues {
  name: string;
  email: string;
  password: string;
  age: number;
}
