import React from 'react';
import { Col, Layout, Row, Card, Typography, message } from 'antd';
import { FormProvider, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import { RegisterValidation } from '@/utils/validation';
import { Form, FieldTextInput, FlexBox } from '@/components';
import { useRegister } from '@/services/Register';

import { RegisterLayout, RegisterButton } from './styled';

import type { IRegisterFormValues } from './type';
import type { AxiosError } from 'axios';
import { useNavigate } from 'react-router-dom';

const { Content } = Layout;
const { Title } = Typography;

const Register = () => {
  const navigate = useNavigate();
  const methods = useForm<IRegisterFormValues>({
    mode: 'onChange',
    resolver: yupResolver(RegisterValidation),
  });

  const { mutate: mutateRegister, isLoading: isLoadingRegister } = useRegister({
    onSuccess: () => {
      message.success('Successfully Register.');
      navigate('/');
    },
    onError: (error: AxiosError) => {
      message.error(error?.message || 'Failed register');
    },
  });

  const onSubmit = (values: IRegisterFormValues) => {
    mutateRegister(values);
  };

  return (
    <RegisterLayout>
      <Content>
        <Row justify="center" gutter={[10, 10]}>
          <Col xs={24} sm={17} md={13} lg={9} xl={7}>
            <Card>
              <FlexBox justifyContent="center">
                <Title>SIGN UP</Title>
              </FlexBox>
              <FormProvider {...methods}>
                <Form onFinish={methods.handleSubmit(onSubmit)}>
                  <FieldTextInput
                    label="Name"
                    name="name"
                    placeholder="Input Name"
                  />
                  <FieldTextInput
                    label="Email"
                    name="email"
                    placeholder="Input Email"
                  />
                  <FieldTextInput
                    type="password"
                    label="Password"
                    name="password"
                    placeholder="Input Password"
                  />
                  <FieldTextInput
                    type="tel"
                    label="Age"
                    name="age"
                    maxLength={3}
                    placeholder="Input Age"
                  />
                  <RegisterButton
                    loading={isLoadingRegister}
                    htmlType="submit"
                    type="primary"
                  >
                    SignUp
                  </RegisterButton>
                </Form>
              </FormProvider>
            </Card>
          </Col>
        </Row>
      </Content>
    </RegisterLayout>
  );
};

export default Register;
