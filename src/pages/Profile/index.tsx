import React, { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { Avatar, Button, Layout, message, Row, Typography, Upload } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { yupResolver } from '@hookform/resolvers/yup';

import { useLocalStorage } from '@/hooks/useLocalStorage';
import { Form, FieldTextInput, FlexBox } from '@/components';
import { UpdateProfileValidation } from '@/utils/validation';
import { useQueryGetProfile, useUpdateProfile } from '@/services/Profile';

import {
  ProfileLayout,
  ProfileContent,
  ProfileDivider,
  ProfileButton,
} from './styled';

import type { AxiosError } from 'axios';
import type { IUpdateProfile } from '@/services/Profile/type';
import type { UploadProps } from 'antd';
import axios from 'axios';

const { Content } = Layout;
const { Title } = Typography;

const Profile = () => {
  const navigate = useNavigate();
  const [user] = useLocalStorage('user', null);
  const [token] = useLocalStorage('token', null);
  const [avatar, setAvatar] = useLocalStorage('avatar', null);
  const [isEdit, setIsEdit] = useState(false);

  const methods = useForm<IUpdateProfile>({
    mode: 'onChange',
    resolver: yupResolver(UpdateProfileValidation),
  });

  const { data: userdata } = useQueryGetProfile();
  console.log(userdata, 'user');

  const { mutate: mutateUpdateProfile, isLoading: isLoadingUpdateProfile } =
    useUpdateProfile({
      onSuccess: () => {
        message.success('Successfully UpdateProfile.');
        navigate('/');
      },
      onError: (error: AxiosError) => {
        message.error(error?.message || 'Failed UpdateProfile');
      },
    });

  const onSubmit = (values: IUpdateProfile) => {
    mutateUpdateProfile(values);
  };

  const props: UploadProps = {
    customRequest: (options: any) => {
      const data = new FormData();
      data.append('file', options.file);
      const config = {
        headers: {
          'content-type': 'multipart/form-data',
          Authorization: `Bearer ${token}`,
        },
      };
      axios
        .post(
          'https://api-nodejs-todolist.herokuapp.com/user/me/avatar',
          data,
          config
        )
        .then((res: any) => {
          options.onSuccess(res.data, options.file);
          setAvatar(res.data);
          message.success('Profile picture successfully uploaded');
        })
        .catch((err: Error) => {
          message.error('Failed to upload profile picture');
          console.log(err);
        });
    },
  };

  return (
    <ProfileLayout>
      <Content className="pt-5 mb-5">
        <Row justify="center" gutter={[10, 10]}>
          <ProfileContent xs={22} sm={14} md={10}>
            <FlexBox justifyContent="space-between" alignItems="center">
              <Title level={2}>My Profile</Title>
              {!isEdit && (
                <Button type="primary" onClick={() => setIsEdit(true)}>
                  Edit
                </Button>
              )}
            </FlexBox>
            <ProfileDivider />
            <FlexBox
              direction="column"
              alignItems="center"
              justifyContent="center"
              columnGap={10}
            >
              <Avatar
                src={avatar}
                icon={<UserOutlined />}
                onClick={() => navigate('/profile')}
              />
              <Upload {...props} accept="image/*" showUploadList={false}>
                <Button type="text">Edit Profile Photo</Button>
              </Upload>
            </FlexBox>
            <FormProvider {...methods}>
              <Form onFinish={methods.handleSubmit(onSubmit)}>
                <FieldTextInput
                  label="Name"
                  name="name"
                  placeholder="Input Name"
                  disabled={!isEdit}
                  defaultValue={user.name}
                />
                <FieldTextInput
                  label="Email"
                  name="email"
                  placeholder="Input Email"
                  disabled
                  defaultValue={user.email}
                />
                <FieldTextInput
                  type="tel"
                  label="Age"
                  name="age"
                  maxLength={3}
                  placeholder="Input Age"
                  disabled={!isEdit}
                  defaultValue={user.age}
                />
                <ProfileButton
                  loading={isLoadingUpdateProfile}
                  htmlType="submit"
                  type="primary"
                  disabled={!isEdit}
                >
                  Edit Profile
                </ProfileButton>

                <ProfileButton
                  htmlType="button"
                  type="default"
                  onClick={() => navigate('/dashboard')}
                >
                  Back to Dashboard
                </ProfileButton>
              </Form>
            </FormProvider>
          </ProfileContent>
        </Row>
      </Content>
    </ProfileLayout>
  );
};

export default Profile;
