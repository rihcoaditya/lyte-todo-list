/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { createContext, useContext, useMemo } from 'react';
import { useNavigate } from 'react-router-dom';
import { useLocalStorage } from './useLocalStorage';
import type { IRoute } from '@/interfaces/routeTypes';

export const AuthContext = createContext('');

export const AuthProvider = ({ children }: IRoute) => {
  const [user, setUser] = useLocalStorage('user', null);
  const navigate = useNavigate();

  const logout = () => {
    setUser(null);
    navigate('/', { replace: true });
  };

  const value: any = useMemo(
    () => ({
      user,
      logout,
    }),
    [user]
  );

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export const useAuth = () => useContext(AuthContext);
