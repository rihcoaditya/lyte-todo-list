/* eslint-disable @typescript-eslint/no-explicit-any */
import { useMutation, useQuery } from 'react-query';

import client from '@/utils/client';

import type { AxiosError } from 'axios';
import type { UseQueryResult, UseMutationResult } from 'react-query';
import type {
  IPostTasks,
  IPostTasksResponse,
  IGetTasksResponse,
  ICompleteTasks,
  ICompleteTasksResponse,
} from './type';

const postTask = async (data: IPostTasks): Promise<IPostTasksResponse> => {
  const response = await client.post<IPostTasksResponse>(`task`, data);
  return response.data;
};
const completeTask = async (
  taskId: string,
  data: ICompleteTasks
): Promise<ICompleteTasksResponse> => {
  const response = await client.put<ICompleteTasksResponse>(
    `task/${taskId}`,
    data
  );
  return response.data;
};

const deleteTask = async (taskId: string): Promise<ICompleteTasksResponse> => {
  const response = await client.delete<ICompleteTasksResponse>(
    `task/${taskId}`
  );

  return response.data;
};

const getTask = async (): Promise<IGetTasksResponse> => {
  const response = await client.get<IGetTasksResponse>(`task`);
  return response.data;
};

const useMutationPostTask = (
  extraParams = {}
): UseMutationResult<
  IPostTasksResponse,
  AxiosError<{ message: string }, any>,
  IPostTasks,
  () => void
> =>
  useMutation<
    IPostTasksResponse,
    AxiosError<{ message: string }, any>,
    IPostTasks,
    () => void
  >((params: IPostTasks) => postTask(params), { ...extraParams });

const useMutationCompleteTask = (
  taskId: string,
  extraParams?: any
): UseMutationResult<
  ICompleteTasksResponse,
  AxiosError<{ message: string }, any>,
  ICompleteTasks,
  () => void
> =>
  useMutation<
    ICompleteTasksResponse,
    AxiosError<{ message: string }, any>,
    ICompleteTasks,
    () => void
  >((params: ICompleteTasks) => completeTask(taskId, params), {
    ...extraParams,
  });

const useMutationDeleteTask = (
  taskId: string,
  extraParams?: any
): UseMutationResult<
  ICompleteTasksResponse,
  AxiosError<{ message: string }, any>,
  undefined,
  () => void
> =>
  useMutation<
    ICompleteTasksResponse,
    AxiosError<{ message: string }, any>,
    undefined,
    () => void
  >(() => deleteTask(taskId), {
    ...extraParams,
  });

const useQueryGetTask = ({
  extraParams,
}: any): UseQueryResult<IGetTasksResponse, AxiosError> =>
  useQuery(['tasks'], () => getTask(), { ...extraParams });

export {
  useMutationPostTask,
  useMutationCompleteTask,
  useMutationDeleteTask,
  useQueryGetTask,
};
