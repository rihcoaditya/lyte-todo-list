export interface IPostTasks {
  description: string;
}
export interface IPostTasksResponse {
  completed: boolean;
  createdAt: string;
  description: string;
  owner: string;
  updatedAt: string;
  _v: number;
  _id: string;
}

export interface IGetTasksResponse {
  count: number;
  data: IPostTasksResponse[];
}

export interface ICompleteTasks {
  completed: boolean;
}

export interface ICompleteTasksResponse {
  completed: boolean;
  createdAt: string;
  description: string;
  owner: string;
  updatedAt: string;
  _v: number;
  _id: string;
}
