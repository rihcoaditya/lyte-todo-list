/* eslint-disable @typescript-eslint/no-explicit-any */
import { useMutation } from 'react-query';

import client from '@/utils/client';

import type { AxiosError } from 'axios';
import type { UseMutationResult } from 'react-query';
import type { IRegister, IRegisterResponse } from './type';

const register = async (data: IRegister): Promise<IRegisterResponse> => {
  const response = await client.post<IRegisterResponse>(`user/register`, data);
  return response.data;
};

const useRegister = (
  extraParams = {}
): UseMutationResult<
  IRegisterResponse,
  AxiosError<{ message: string }, any>,
  IRegister,
  () => void
> =>
  useMutation<
    IRegisterResponse,
    AxiosError<{ message: string }, any>,
    IRegister,
    () => void
  >((params: IRegister) => register(params), { ...extraParams });

export { useRegister };
