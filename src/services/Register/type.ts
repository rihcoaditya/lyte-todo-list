export interface IRegister {
  name: string;
  email: string;
  password: string;
  age: number;
}

export interface IRegisterResponse {
  token: string;
  user: {
    age: number;
    createdAt: string;
    email: string;
    name: string;
    _id: string;
  };
}
