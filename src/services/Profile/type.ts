export interface IUpdateProfile {
  name: string;
  age: number;
}

export interface IUpdateProfileResponse {
  token: string;
  user: {
    age: number;
    createdAt: string;
    email: string;
    name: string;
    _id: string;
  };
}
