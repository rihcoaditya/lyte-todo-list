/* eslint-disable @typescript-eslint/no-explicit-any */
import { useQuery, useMutation } from 'react-query';

import client from '@/utils/client';

import type { ILoginResponse } from '../Login/type';
import type { AxiosError } from 'axios';
import type { UseQueryResult, UseMutationResult } from 'react-query';
import type { IUpdateProfile, IUpdateProfileResponse } from './type';

const getProfilePicture = async (payload: any): Promise<IUpdateProfile> => {
  const response = await client.get<IUpdateProfile>(
    `user/${payload.userId}/avatar`
  );

  return response.data;
};

const getProfile = async (): Promise<ILoginResponse> => {
  const response = await client.get<ILoginResponse>(`user/me`);

  return response.data;
};

const updateProfile = async (
  data: IUpdateProfile
): Promise<IUpdateProfileResponse> => {
  const response = await client.post<IUpdateProfileResponse>(`user/me`, data);
  return response.data;
};

const useQueryGetProfilePicture = (
  payload: any,
  extraParams?: object
): UseQueryResult<any, AxiosError> =>
  useQuery(['profilepicture'], () => getProfilePicture(payload), {
    ...extraParams,
  });

const useQueryGetProfile = (
  extraParams?: object
): UseQueryResult<any, AxiosError> =>
  useQuery(['profile'], () => getProfile(), { ...extraParams });

const useUpdateProfile = (
  extraParams = {}
): UseMutationResult<
  IUpdateProfileResponse,
  AxiosError<{ message: string }, any>,
  IUpdateProfile,
  () => void
> =>
  useMutation<
    IUpdateProfileResponse,
    AxiosError<{ message: string }, any>,
    IUpdateProfile,
    () => void
  >((params: IUpdateProfile) => updateProfile(params), { ...extraParams });

export { useQueryGetProfilePicture, useQueryGetProfile, useUpdateProfile };
