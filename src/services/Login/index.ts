/* eslint-disable @typescript-eslint/no-explicit-any */
import { useMutation } from 'react-query';

import client from '@/utils/client';

import type { AxiosError } from 'axios';
import type { UseMutationResult } from 'react-query';
import type { ILogin, ILoginResponse } from './type';

const login = async (data: ILogin): Promise<ILoginResponse> => {
  const response = await client.post<ILoginResponse>(`user/login`, data);

  return response.data;
};

const logout = async (): Promise<{ success: boolean }> => {
  const response = await client.post<{ success: boolean }>(`user/logout`);

  return response.data;
};

const useLogin = (
  extraParams = {}
): UseMutationResult<
  ILoginResponse,
  AxiosError<{ message: string }, any>,
  ILogin,
  () => void
> =>
  useMutation<
    ILoginResponse,
    AxiosError<{ message: string }, any>,
    ILogin,
    () => void
  >((params: ILogin) => login(params), { ...extraParams });

const useLogout = (
  extraParams = {}
): UseMutationResult<
  { success: boolean },
  AxiosError<{ message: string }, any>,
  undefined,
  () => void
> =>
  useMutation<
    { success: boolean },
    AxiosError<{ message: string }, any>,
    undefined,
    () => void
  >(() => logout(), { ...extraParams });

export { useLogin, useLogout };
