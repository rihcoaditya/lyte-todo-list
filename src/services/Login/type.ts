export interface ILogin {
  email: string;
  password: string;
}
export interface ILoginResponse {
  token: string;
  user: {
    age: number;
    createdAt: string;
    email: string;
    name: string;
    _id: string;
  };
}
