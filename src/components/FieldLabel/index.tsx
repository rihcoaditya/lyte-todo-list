import React, { useMemo } from 'react';
import { Typography } from 'antd';

import type { FieldLabelProps } from './type';

const { Text: AntdText } = Typography;

const FieldLabel: React.FC<FieldLabelProps> = ({
  title = '',
  isRequired,
  className,
  ...restProps
}) => {
  const renderAsterisk = useMemo(() => {
    if (!isRequired) return '';
    return <AntdText type="danger">*</AntdText>;
  }, [isRequired]);

  return (
    <AntdText {...restProps} className={className}>
      {title}
      {renderAsterisk}
    </AntdText>
  );
};

export default FieldLabel;
