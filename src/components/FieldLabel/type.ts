export interface FieldLabelProps {
  isRequired?: boolean;
  title?: string;
  className?: string;
}
