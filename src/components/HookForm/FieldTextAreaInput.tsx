/* eslint-disable no-unused-vars */
import React from 'react';
import { Input } from 'antd';
import { useFormContext, Controller } from 'react-hook-form';

import { get } from 'lodash';

import { FieldLabel, FormItem } from '@/components';

import type { ChangeEvent } from 'react';
import type { TextAreaProps } from 'antd/lib/input/TextArea';

const { TextArea } = Input;

export interface ShowCountProps {
  formatter: (args: { count: number; maxLength?: number }) => string;
}

export interface IFieldTextAreaInput extends Omit<TextAreaProps, 'onChange'> {
  name: string;
  defaultValue?: string;
  placeholder?: string;
  isRequired?: string;
  type?: string;
  maxLength?: number;
  label?: string;
  withMargin?: boolean;
  showCount?: boolean | ShowCountProps;
  onChange?: (event: ChangeEvent<HTMLTextAreaElement>) => void;
}

export const FieldTextAreaInput: React.FC<IFieldTextAreaInput> = ({
  name,
  label,
  defaultValue,
  placeholder,
  maxLength,
  withMargin = true,
  onChange,
  ...restProps
}) => {
  const {
    control,
    formState: { errors },
  } = useFormContext();

  const isError = get(errors, name);

  return (
    <>
      {label && <FieldLabel title={label} className="mb-2 block" />}
      <FormItem
        isError={isError}
        help={(isError?.message as string) || null}
        className={withMargin ? 'mb-4' : 'mb-0'}
      >
        <Controller
          name={name}
          defaultValue={defaultValue}
          control={control}
          render={({ field }) => (
            <TextArea
              {...field}
              placeholder={placeholder}
              maxLength={maxLength}
              showCount
              onChange={(value: ChangeEvent<HTMLTextAreaElement>) => {
                if (onChange) return onChange(value);
                return field.onChange(value);
              }}
              {...restProps}
            />
          )}
        />
      </FormItem>
    </>
  );
};

export default FieldTextAreaInput;
