import React from 'react';
import { Select } from 'antd';
import { get } from 'lodash';
import { useFormContext, Controller } from 'react-hook-form';

import { FormItem, FieldLabel } from '@/components';

const { Option } = Select;

export interface IFieldSelectInput {
  name: string;
  placeholder?: string;
  label?: string;
  children?: React.ReactNode;
  isLoading?: boolean;
  withMargin?: boolean;
  className?: string;
}

const FieldSelectInput: React.FC<IFieldSelectInput> = ({
  name,
  placeholder,
  label,
  children,
  isLoading = false,
  className,
}) => {
  const {
    control,
    formState: { errors },
  } = useFormContext();

  const isError = React.useMemo(() => get(errors, name), [errors, name]);

  return (
    <>
      {label && <FieldLabel title={label} className="mb-2 block" />}
      <FormItem
        isError={isError}
        help={(isError?.message as string) || null}
        className={className}
      >
        <Controller
          name={name}
          control={control}
          render={({ field }) => (
            <Select
              {...field}
              placeholder={placeholder}
              defaultValue={field.value}
              getPopupContainer={(triggerNode) => triggerNode.parentElement}
              loading={isLoading}
            >
              {children}
            </Select>
          )}
        />
      </FormItem>
    </>
  );
};

export default Object.assign(FieldSelectInput, {
  Option,
});
