/* eslint-disable no-unused-vars */
import React from 'react';

import { useFormContext, Controller } from 'react-hook-form';

import { get } from 'lodash';

import { FieldLabel, FormItem, Input } from '@/components';

export interface IFieldTextInput {
  name: string;
  defaultValue?: string;
  placeholder?: string;
  type?: string;
  maxLength?: number;
  label?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  disabled?: boolean;
  onPaste?: React.ClipboardEventHandler<HTMLInputElement>;
  className?: string;
}

export const FieldTextInput: React.FC<IFieldTextInput> = ({
  name,
  label,
  defaultValue,
  placeholder,
  maxLength,
  type = 'text',
  onChange,
  disabled,
  onPaste,
  className,
}) => {
  const {
    control,
    formState: { errors },
  } = useFormContext();

  const isError = get(errors, name);

  return (
    <>
      {label && <FieldLabel title={label} className="mb-2 block" />}
      <FormItem
        isError={isError}
        help={(isError?.message as string) || null}
        className={className}
      >
        <Controller
          name={name}
          defaultValue={defaultValue}
          control={control}
          render={({ field }) => (
            <Input
              {...field}
              onPaste={onPaste}
              placeholder={placeholder}
              type={type}
              maxLength={maxLength}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                if (onChange) return onChange(event);
                return field.onChange(event);
              }}
              disabled={disabled}
            />
          )}
        />
      </FormItem>
    </>
  );
};

export default FieldTextInput;
