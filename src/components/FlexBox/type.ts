export interface FlexBoxProps {
  width?: string;
  justifyContent?:
    | 'flex-start'
    | 'flex-end'
    | 'center'
    | 'space-between'
    | 'space-around'
    | 'space-evenly';
  alignItems?: 'flex-start' | 'flex-end' | 'center' | 'baseline' | 'stretch';
  direction?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
  columnGap?: number;
  rowGap?: number;
  wrap?: 'nowrap' | 'wrap' | 'wrap-reverse';
  height?: string;
  backgroundColor?: string;
}
