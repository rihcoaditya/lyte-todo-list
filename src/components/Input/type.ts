import type { InputProps as AntdInputProps } from 'antd';

export type SizeType = 'sm' | 'md' | 'lg';

export interface InputProps extends Omit<AntdInputProps, 'size'> {
  name?: string;
  value?: number | string;
  placeholder?: string;
  defaultValue?: number | string;
  isDisabled?: boolean;
  type?: string;
}
