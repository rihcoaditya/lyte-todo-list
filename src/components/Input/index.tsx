import React from 'react';

import { Input as AntdInput } from 'antd';

import type { InputProps } from './type';

export const Input: React.FC<InputProps> = ({
  name,
  defaultValue,
  value,
  placeholder,
  isDisabled,
  onChange,
  type,
  ...restProps
}) => {
  const InputType = type !== 'password' ? AntdInput : AntdInput.Password;
  return (
    <InputType
      name={name}
      defaultValue={defaultValue}
      value={value}
      placeholder={placeholder}
      onChange={onChange}
      disabled={isDisabled}
      type={type}
      {...restProps}
    />
  );
};

export default Input;
