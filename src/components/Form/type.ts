import { FormProps as AntdFormProps } from 'antd';
export interface FormsProps extends AntdFormProps {
  children: React.ReactNode;
  layout?: 'horizontal' | 'vertical' | 'inline';
}
