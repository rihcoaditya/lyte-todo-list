import { Form as AntdForm } from 'antd';

import type { FormsProps } from './type';

const Form = ({ children, layout = 'vertical', ...rest }: FormsProps) => (
  <AntdForm layout={layout} {...rest}>
    {children}
  </AntdForm>
);

const { useForm, Provider, List, useWatch } = AntdForm;

Form.useForm = useForm;
Form.useWatch = useWatch;
Form.Provider = Provider;
Form.List = List;

export default Form;
