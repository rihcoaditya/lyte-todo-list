/* eslint-disable @typescript-eslint/no-explicit-any */
import { Navigate } from 'react-router-dom';
import { useAuth } from '@/hooks/useAuth';
import type { IRoute } from '@/interfaces/routeTypes';

export const ProtectedRoute = ({ children }: IRoute) => {
  const { user }: any = useAuth();

  if (!user) {
    return <Navigate to="/" />;
  }

  return children;
};
