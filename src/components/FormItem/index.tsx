import React from 'react';
import { Form } from 'antd';

import type { FormItemProps } from './type';

const FormItem: React.FC<FormItemProps> = ({
  children,
  label = '',
  isError = false,
  className,
  ...restProps
}) => (
  <>
    <Form.Item
      label={label}
      colon={false}
      validateStatus={isError ? 'error' : undefined}
      className={className}
      {...restProps}
    >
      {children}
    </Form.Item>
  </>
);

export default FormItem;
