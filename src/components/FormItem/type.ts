import type React from 'react';
import type { FormItemProps as AntdFormItemProps } from 'antd';
import type { FieldError, FieldErrorsImpl, Merge } from 'react-hook-form';

export interface FormItemProps extends AntdFormItemProps {
  children: React.ReactNode;
  label?: string | React.ReactNode;
  isError?: boolean | FieldError | Merge<FieldError, FieldErrorsImpl<any>>;
  className?: string;
}
