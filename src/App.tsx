import { useState } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { AuthProvider } from './hooks/useAuth';
import MainRoutes from './routes/MainRoute';

function App() {
  const [queryClient] = useState(
    () =>
      new QueryClient({
        defaultOptions: {
          queries: {
            refetchOnWindowFocus: false,
          },
        },
      })
  );
  return (
    <>
      <QueryClientProvider client={queryClient}>
        <AuthProvider>
          <MainRoutes />
        </AuthProvider>
      </QueryClientProvider>
    </>
  );
}

export default App;
